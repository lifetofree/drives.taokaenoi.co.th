using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_sap_link_file : System.Web.UI.Page {
    string value = String.Empty;

    private void Page_Init (object sender, EventArgs e) {
        value = (string) RouteData.Values["file_name"];

        if (Session["emp_idx"] == null) {
            string path = HttpContext.Current.Request.Url.AbsolutePath;
            Response.Redirect (ResolveUrl ("~/warning") + "?url=" + path);
            return;
        }
    }

    protected void Page_Load (object sender, EventArgs e) {
        if (value != String.Empty) {
            string FilePath = Server.MapPath ("/uploadfiles/sap/" + value + ".pdf");
            WebClient User = new WebClient ();
            Byte[] FileBuffer = User.DownloadData (FilePath);
            if (FileBuffer != null) {
                Response.ContentType = "application/pdf";
                Response.AddHeader ("content-length", FileBuffer.Length.ToString ());
                Response.BinaryWrite (FileBuffer);
            }
        }
    }
}