﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Collections;

public partial class websystem_login : System.Web.UI.Page
{
    #region initial function/data
    function_tool _funcTool = new function_tool();
    data_employee _data_employee = new data_employee();

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string _urlCheckLogin = _serviceUrl + ConfigurationManager.AppSettings["urlCheckLogin"];

    string _localJson = "";
	int _tempInt = 0;
    string url = "";
    #endregion initial function/data

    private void Page_Init(object sender, System.EventArgs e)
    {
        ClearApplicationCache();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        tbEmpCode.Focus();
        url = Request.QueryString["url"];

        if(!IsPostBack)
        {
            Session.Clear();
            divShowError.Visible = false;
        }
    }

    #region btn command
    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();

        switch (cmdName)
        {
            case "cmdLogin":
                string _empCode = tbEmpCode.Text.Trim();
                string _empPass = _funcTool.getMd5Sum(tbEmpPass.Text.Trim());
                if(_empCode != String.Empty && _empPass != String.Empty)
                {
                    // set data
                    _data_employee.employee_list = new employee_detail[1];
                    employee_detail _empDetail = new employee_detail();
                    _empDetail.emp_code = _empCode;
                    _empDetail.emp_password = _empPass;
                    _data_employee.employee_list[0] = _empDetail;

                    _data_employee = callServiceEmployee(_urlCheckLogin, _data_employee);
                    // litDebug.Text = _funcTool.convertObjectToJson(_data_employee);

                    // check return_code
                    if(int.Parse(_data_employee.return_code) == 0)
                    {
                        // create session
                        Session["emp_idx"] = _data_employee.employee_list[0].emp_idx;
                        Response.Redirect(ResolveUrl("~/" + url));
                    }
                    else
                    {
                        divShowError.Visible = !divShowError.Visible;
                        litErrorCode.Text = _data_employee.return_code.ToString();
                        //litDebug.Text = "error : " + _data_employee.return_code.ToString() + " - " + _data_employee.return_msg;
                    }
                }
            break;
        }
    }
    #endregion btn command

    protected data_employee callServiceEmployee(string _cmdUrl, data_employee _data_employee)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_data_employee);

        // call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        // convert json to object
        _data_employee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);


        return _data_employee;
    }

    #region reuse
    public void ClearApplicationCache()
    {
        List<string> keys = new List<string>();
        // retrieve application Cache enumerator
        IDictionaryEnumerator enumerator = Cache.GetEnumerator();
        // copy all keys that currently exist in Cache
        while (enumerator.MoveNext())
        {
            keys.Add(enumerator.Key.ToString());
        }

        // delete every key from cache
        for (int i = 0; i < keys.Count; i++)
        {
            Cache.Remove(keys[i]);
        }
    }
    #endregion
}
