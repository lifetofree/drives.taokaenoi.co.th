using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_success : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string url = Request.QueryString["url"];

        if (url == null)
        {
			Response.AddHeader("REFRESH", "3;URL=" + ResolveUrl("~/"));
        }
        else
        {
            Response.AddHeader("REFRESH", "3;URL=" + ResolveUrl("~/") + "?url=" + url);
        }
    }
}
