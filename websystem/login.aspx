﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="login.aspx.cs" Inherits="websystem_login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">

<head runat="server">
    <title>MAS</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <!-- <link href='https://fonts.googleapis.com/css?family=Kanit' rel='stylesheet'> -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css"
        integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <link href="./../Content/bootstrap.css" runat="server" rel="stylesheet" />
    <link href="./../Content/custom.css" runat="server" rel="stylesheet" />
</head>

<body>
    <form id="formMaster" runat="server">
        <asp:ScriptManager ID="tsmMaster" runat="server"></asp:ScriptManager>
        <script src='<%= ResolveUrl("~/Scripts/jquery-3.3.1.min.js") %>'></script>
        <script src='<%= ResolveUrl("~/Scripts/bootstrap.js") %>'></script>

        <asp:UpdatePanel ID="upLogin" runat="server">
            <ContentTemplate>
                <asp:Literal ID="litDebug" runat="server"></asp:Literal>
                    <div class="row vertical-offset-50 justify-content-md-center">
                        <div class="col-md-6 col-md-offset-3">
                            <div class="col-md-12">
                                <div class="text-center">
                                    <img src='<%= ResolveUrl("~/masterpage/images/logo_mas_01.png") %>'
                                        class="img-fluid" width="50%" alt="MAS" />
                                </div>
                                <div class="row bg-login display-flex">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <asp:TextBox ID="tbEmpCode" runat="server"
                                                CssClass="form-control form-control-lg bg-control" placeholder="Employee Code"
                                                MaxLength="8" ValidationGroup="formLogin"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfvEmpCode" ValidationGroup="formLogin"
                                                runat="server" Display="None" SetFocusOnError="true"
                                                ControlToValidate="tbEmpCode" ErrorMessage="กรุณากรอกรหัสพนักงาน" />
                                            <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="vceEmpCode"
                                                TargetControlID="rfvEmpCode"
                                                HighlightCssClass="validatorCalloutHighlight" />
                                        </div>
                                        <div class="form-group">
                                            <asp:TextBox ID="tbEmpPass" runat="server"
                                                CssClass="form-control form-control-lg bg-control" placeholder="Password"
                                                MaxLength="20" TextMode="Password" ValidationGroup="formLogin">
                                            </asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfvEmpPass" ValidationGroup="formLogin"
                                                runat="server" Display="None" SetFocusOnError="true"
                                                ControlToValidate="tbEmpPass" ErrorMessage="กรุณากรอกรหัสผ่าน" />
                                            <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="vceEmpPass"
                                                TargetControlID="rfvEmpPass"
                                                HighlightCssClass="validatorCalloutHighlight" />
                                        </div>

                                        <div class="form-group">
                                            <asp:Button ID="btnLogin" CssClass="btn btn-lg btn-success btn-block"
                                                runat="server" data-original-title="Login" data-toggle="tooltip"
                                                OnCommand="btnCommand" CommandName="cmdLogin" Text="Login"
                                                ValidationGroup="formLogin" />
                                        </div>
                                    </div>
                                </div>
                                <div id="divShowError" runat="server" class="alert alert-danger"
                                    style="margin-top: 10px;" role="alert">
                                    <strong>Error
                                        <asp:Literal ID="litErrorCode" runat="server"></asp:Literal>
                                        : </strong>
                                    รหัสพนักงาน หรือ รหัสผ่าน ไม่ถูกต้อง
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        <div>
            <asp:UpdateProgress ID="UpdateProgress" runat="server">
                <ProgressTemplate>
                    <div id="modalPopup" runat="server" class="modalPopup">
                        <div class="centerPopup">
                            <asp:Image ID="imgWaiting" ImageUrl="~/masterpage/images/loading.gif"
                                AlternateText="Processing" runat="server" Width="50" Height="50" />
                        </div>
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
        </div>
    </form>
</body>

</html>