﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="warning.aspx.cs" Inherits="websystem_warning" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>MAS</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link href="./../Content/bootstrap.css" runat="server" rel="stylesheet" />
    <link href="./../Content/custom.css" runat="server" rel="stylesheet" />

    <link rel="shortcut icon" href="./../images/Logo_TKN-01.png" type="image/x-icon" />
    <%--<link rel="icon" href="./../images/favicontkn.ico" type="image/x-icon" />--%>
    <link rel="icon" type="image/png" sizes="16x16" href="./../images/Logo_TKN-01.png" />

</head>
<body>
    <form id="formMaster" runat="server">
		<asp:ScriptManager ID="tsmMaster" runat="server"></asp:ScriptManager>
		<script src='<%=ResolveUrl("~/Scripts/jquery-3.1.1.min.js") %>'></script>
		<script src='<%=ResolveUrl("~/Scripts/bootstrap.js") %>'></script>
		<script src='<%=ResolveUrl("~/Scripts/custom.js") %>'></script>

        <asp:Literal ID="litDebug" runat="server"></asp:Literal>
        <div class="container">
            <div class="row vertical-offset-100 justify-content-md-center">
                <div class="col-md-6 col-md-offset-3">
                    
                    <div ID="divShowWarning" runat="server" class="alert alert-warning" role="alert">
                        <strong>Warning!!! : </strong>
                        หมดเวลาการเชื่อมต่อระบบ กรุณารอสักครู่ ระบบจะพาท่านไปยังหน้า login เพื่อลองเชื่อมต่อระบบอีกครั้ง
                    </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
