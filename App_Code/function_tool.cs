using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Text;
using System.Security.Cryptography;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Net;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Xml.Linq;
using Newtonsoft.Json;
using System.Configuration;
using System.Globalization;

/// <summary>
/// Summary description for function_tool
/// </summary>
public class function_tool
{
    public function_tool()
    {
    }

    #region random, MD5
    public string getRandomPasswordUsingGuid(int length)
    {
        // Get the GUID
        string guidResult = System.Guid.NewGuid().ToString();

        // Remove the hyphens
        guidResult = guidResult.Replace("-", string.Empty);

        // Make sure length is valid
        if (length <= 0 || length > guidResult.Length)
            throw new ArgumentException("Length must be between 1 and " + guidResult.Length);

        // Return the first length bytes
        return guidResult.Substring(0, length);
    }

    public string getMd5Sum(string str)
    {
        byte[] input = ASCIIEncoding.ASCII.GetBytes(str);
        byte[] output = MD5.Create().ComputeHash(input);
        var sb = new StringBuilder();
        foreach (byte t in output)
        {
            sb.Append(t.ToString("X2").ToLower());
        }
        return sb.ToString();
    }
    #endregion

    #region display alert
    public void showAlert(Control control, string strMessage)
    {
        if (!control.Page.ClientScript.IsClientScriptBlockRegistered(Guid.NewGuid().ToString()))
        {
            var script = String.Format("<script type='text/javascript' language='javascript'>alert('{0}')</script>", strMessage);
            control.Page.ClientScript.RegisterClientScriptBlock(control.Page.GetType(), "PopupScript", script);
        }
    }
    #endregion

    #region display other javascript
    public void showOtherJavaScript(Control control, string strMessage)
    {
        if (!control.Page.ClientScript.IsClientScriptBlockRegistered(Guid.NewGuid().ToString()))
        {
            var script = String.Format("<script type='text/javascript' language='javascript'>{0}</script>", strMessage);
            control.Page.ClientScript.RegisterClientScriptBlock(control.Page.GetType(), "PopupScriptII", script);
        }
    }
    #endregion

    #region convert data
    public DateTime getSmallDateTime(string dateIn)
    {
        DateTime dateDefault;

        DateTime dateOut = DateTime.TryParse(dateIn, out dateDefault) ? DateTime.Parse(dateIn) : dateDefault;

        return dateOut;
    }

    public string getOnlyDate(string dateIn)
    {
        string[] dateOut = dateIn.Split(' ');

        return dateOut[0];
    }

    public int convertToInt(string dataIn)
    {
        int _default_int = 0;
        return int.TryParse(dataIn, out _default_int) ? int.Parse(dataIn) : _default_int;
    }

    public decimal convertToDecimal(string dataIn)
    {
        decimal _default_decimal = 0;
        NumberStyles style = NumberStyles.AllowDecimalPoint;
        CultureInfo culture = CultureInfo.CreateSpecificCulture("en-EN");
        return decimal.TryParse(dataIn, style, culture, out _default_decimal) ? decimal.Parse(dataIn, style, culture) : _default_decimal;
    }

    public decimal setDecimalPlaces(decimal dataIn, int dataPlaces)
    {
        decimal dataOut = 0;
        try { dataOut = decimal.Round(dataIn, dataPlaces, MidpointRounding.AwayFromZero); }
        catch { dataOut = 0; }
        return dataOut;
    }
    #endregion

    #region calc date difference
    public Double calcDateDiff(string diffType, DateTime startDate, DateTime endDate)
    {
        Double outData = 0;
        try
        {
            TimeSpan ts = endDate - startDate;
            switch (diffType)
            {
                case "day":
                    outData = ts.TotalDays;
                    break;
            }
        }
        catch
        {
        }

        return outData;
    }

    public string calYearMonthDay(DateTime startDate, DateTime endDate)
    {
        int years;
        int months;
        int days;
        int hours;
        int minutes;
        int seconds;
        int milliseconds;

        //------------------
        // Handle the years.
        //------------------
        years = endDate.Year - startDate.Year;

        //------------------------
        // See if we went too far.
        //------------------------
        DateTime test_date = startDate.AddMonths(12 * years);

        if (test_date > endDate)
        {
            years--;
            test_date = startDate.AddMonths(12 * years);
        }

        //--------------------------------
        // Add months until we go too far.
        //--------------------------------
        months = 0;

        while (test_date <= endDate)
        {
            months++;
            test_date = startDate.AddMonths(12 * years + months);
        }

        months--;

        //------------------------------------------------------------------
        // Subtract to see how many more days, hours, minutes, etc. we need.
        //------------------------------------------------------------------
        startDate = startDate.AddMonths(12 * years + months);
        TimeSpan remainder = endDate - startDate;
        days = remainder.Days;
        // hours = remainder.Hours;
        // minutes = remainder.Minutes;
        // seconds = remainder.Seconds;
        // milliseconds = remainder.Milliseconds;

        return (years > 0 ? years.ToString() + " years " : "") +
             (months > 0 ? months.ToString() + " months " : "") +
             (days > 0 ? days.ToString() + " days " : "");
    }
    #endregion

    #region gridview display
    public void makeGridViewPrinterFriendly(GridView gridView)
    {
        if (gridView.Rows.Count > 0)
        {
            gridView.UseAccessibleHeader = true;
            gridView.HeaderRow.TableSection = TableRowSection.TableHeader;
        }
    }

    public void makeEmptyGridviewFix(GridView grdView)
    {
        // normally executes after a grid load method
        if (grdView.Rows.Count == 0 &&
            grdView.DataSource != null)
        {
            DataTable dt = null;

            // need to clone sources otherwise it will be indirectly adding to 
            // the original source

            var set = grdView.DataSource as DataSet;
            if (set != null)
            {
                dt = set.Tables[0].Clone();
            }
            else
            {
                var table = grdView.DataSource as DataTable;
                if (table != null)
                {
                    dt = table.Clone();
                }
            }

            if (dt == null)
            {
                return;
            }

            dt.Rows.Add(dt.NewRow()); // add empty row
            grdView.DataSource = dt;
            grdView.DataBind();

            // hide row
            grdView.Rows[0].Visible = false;
            grdView.Rows[0].Controls.Clear();
        }

        // normally executes at all postbacks
        if (grdView.Rows.Count == 1 &&
            grdView.DataSource == null)
        {
            bool bIsGridEmpty = true;

            // check first row that all cells empty
            for (int i = 0; i < grdView.Rows[0].Cells.Count; i++)
            {
                if (grdView.Rows[0].Cells[i].Text != string.Empty)
                {
                    bIsGridEmpty = false;
                }
            }
            // hide row
            if (bIsGridEmpty)
            {
                grdView.Rows[0].Visible = false;
                grdView.Rows[0].Controls.Clear();
            }
        }
    }
    #endregion

    #region convert custom array object
    public string convertObjectToXml(Object objData)
    {
        try
        {
            var xmlDoc = new XmlDocument(); //Represents an XML document, 
                                            // Initializes a new instance of the XmlDocument class.          
            var xmlSerializer = new XmlSerializer(objData.GetType());
            // Create empty namespace
            var namespaces = new XmlSerializerNamespaces();
            namespaces.Add(string.Empty, string.Empty);
            // Creates a stream whose backing store is memory. 
            using (var xmlStream = new MemoryStream())
            {
                xmlSerializer.Serialize(xmlStream, objData, namespaces);
                xmlStream.Position = 0;
                //Loads the XML document from the specified string.
                xmlDoc.Load(xmlStream);
                foreach (XmlNode node in xmlDoc)
                {
                    if (node.NodeType == XmlNodeType.XmlDeclaration)
                    {
                        xmlDoc.RemoveChild(node);
                    }
                }
                return xmlDoc.InnerXml;
            }
        }
        catch (Exception ex)
        {
            return ex.Message;
        }

    }

    public Object convertXmlToObject(Type dataName, string xmlText)
    {
        try
        {
            var deserializer = new XmlSerializer(dataName);
            TextReader reader = new StringReader(xmlText);
            Object retData = deserializer.Deserialize(reader);
            reader.Close();

            return retData;
        }
        catch (Exception ex)
        {
            throw;
        }
    }
    #endregion

    #region XML to JSON and JSON to XML
    public string convertXmlToJson(string dataXml)
    {
        try
        {
            var doc = new XmlDocument();
            doc.LoadXml(dataXml);
            return JsonConvert.SerializeXmlNode(doc);
        }
        catch (Exception ex)
        {
            throw;
        }
    }

    public string convertJsonToXml(string dataJson)
    {
        try
        {
            XNode node = JsonConvert.DeserializeXNode(dataJson);
            return node.ToString();
        }
        catch (Exception)
        {
            throw;
        }
    }
    #endregion

    #region Json to Object and Object to Json
    public Object convertJsonToObject(Type dataName, string dataJson)
    {
        try
        {
            // convert Json to Xml
            string _localJson = convertJsonToXml(dataJson);
            // convert Xml to Object
            return convertXmlToObject(dataName, _localJson);
        }
        catch (Exception)
        {
            throw;
        }
    }

    public string convertObjectToJson(Object objData)
    {
        try
        {
            // convert Object to Xml
            string _localXml = convertObjectToXml(objData);
            // convert Xml to Json
            return convertXmlToJson(_localXml);
        }
        catch (Exception)
        {
            throw;
        }
    }
    #endregion Json to Object and Object to Json

    #region call web services
    // Returns JSON string
    public string callServiceGet(string url)
    {
        HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
        try
        {
            WebResponse response = request.GetResponse();
            using (Stream responseStream = response.GetResponseStream())
            {
                StreamReader reader = new StreamReader(responseStream, Encoding.UTF8);
                return reader.ReadToEnd();
            }
        }
        catch (WebException ex)
        {
            WebResponse errorResponse = ex.Response;
            using (Stream responseStream = errorResponse.GetResponseStream())
            {
                StreamReader reader = new StreamReader(responseStream, Encoding.GetEncoding("utf-8"));
                String errorText = reader.ReadToEnd();
                // log errorText
            }
            throw;
        }
    }

    // POST a JSON string
    public string callServicePost(string url, string jsonContent)
    {
        HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
        request.Method = "POST";

        string postData = "jsonIn=" + convEncodeUrl(jsonContent);
        UTF8Encoding encoding = new UTF8Encoding();
        var data = encoding.GetBytes(postData);

        request.ContentType = @"application/x-www-form-urlencoded; charset=utf-8";
        request.ContentLength = data.Length;

        using (Stream dataStream = request.GetRequestStream())
        {
            dataStream.Write(data, 0, data.Length);
        }

        long length = 0;
        try
        {
            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            {
                length = response.ContentLength;
                using (Stream responseStream = response.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(responseStream, Encoding.UTF8);
                    return reader.ReadToEnd();
                }
            }
        }
        catch (WebException ex)
        {
            // Log exception and throw as for GET example above
            WebResponse errorResponse = ex.Response;
            using (Stream responseStream = errorResponse.GetResponseStream())
            {
                StreamReader reader = new StreamReader(responseStream, Encoding.GetEncoding("utf-8"));
                String errorText = reader.ReadToEnd();
                // log errorText
            }
            throw;
        }
    }
    #endregion call web services

    #region convert url data
    public string convEncodeUrl(string data_in)
    {
        return HttpUtility.UrlEncode(data_in);
    }
    #endregion convert url data

    #region encoding and decoding w/ rc4
    public string getDecryptRC4(string dataIn, string txtKey)
    {
        string dataOut;
        try
        {
            rc4 rc4Text = new rc4(txtKey, dataIn);
            rc4Text.Text = rc4.hexStrToStr(dataIn);
            dataOut = rc4Text.enDeCrypt();
        }
        catch
        {
            dataOut = "0";
        }

        return dataOut;
    }

    public string getEncryptRC4(string dataIn, string txtKey)
    {
        string dataOut;
        try
        {
            rc4 rc4Text = new rc4(txtKey, dataIn);
            dataOut = rc4.strToHexStr(rc4Text.enDeCrypt()).ToLower();
        }
        catch
        {
            dataOut = "0";
        }

        return dataOut;
    }
    #endregion encoding and decoding w/ rc4

    #region pretty print json/XML
    public string prettyPrint(string s_input)
    {
        if (string.IsNullOrEmpty(s_input))
        {
            return s_input;
        }

        try
        {
            return XDocument.Parse(s_input).ToString();
        }
        catch (Exception) { }

        try
        {
            var t = JsonConvert.DeserializeObject<object>(s_input);
            return JsonConvert.SerializeObject(t, Newtonsoft.Json.Formatting.Indented);
        }
        catch (Exception) { }

        return s_input;
    }
    #endregion pretty print json/XML

    #region check directory, image
    public bool checkDir(string dirName)
    {
        if (!Directory.Exists(HttpContext.Current.Server.MapPath(dirName)))
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public bool checkFile(string fileName)
    {
        if (!File.Exists(HttpContext.Current.Server.MapPath(fileName)))
        {
            return false;
        }
        else
        {
            return true;
        }
    }
    #endregion check directory, image

    #region binding object
    public void setGvData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }

    public void setFvData(FormView fvName, FormViewMode fvMode, Object obj)
    {
        fvName.ChangeMode(fvMode);
        fvName.DataSource = obj;
        fvName.DataBind();
    }

    public void setRptData(Repeater rptName, Object obj)
    {
        rptName.DataSource = obj;
        rptName.DataBind();
    }

    public void setDdlData(DropDownList ddlName, Object obj, string _data_text, string _data_value)
    {
        // clear data
        ddlName.Items.Clear();
        // bind items
        ddlName.DataSource = obj;
        ddlName.DataTextField = _data_text;
        ddlName.DataValueField = _data_value;
        ddlName.DataBind();
    }

    public void setRblData(RadioButtonList rblName, Object obj, string _data_text, string _data_value)
    {
        // clear data
        rblName.Items.Clear();
        // bind items
        rblName.DataSource = obj;
        rblName.DataTextField = _data_text;
        rblName.DataValueField = _data_value;
        rblName.DataBind();
    }

    public void setCblData(CheckBoxList cblName, Object obj, string _data_text, string _data_value)
    {
        // clear data
        cblName.Items.Clear();
        // bind items
        cblName.DataSource = obj;
        cblName.DataTextField = _data_text;
        cblName.DataValueField = _data_value;
        cblName.DataBind();
    }
    #endregion binding object

    #region upload FTP
    public string uploadToFtp(string uploadUrl, string uploadUserName, string uploadPassword, string fileToUpload, string fileName)
    {
        try
        {
            var ftpRequest = (FtpWebRequest)WebRequest.Create(uploadUrl + @"/" + fileName);
            ftpRequest.Method = WebRequestMethods.Ftp.UploadFile;

            ftpRequest.UseBinary = true;
            ftpRequest.Proxy = null;
            ftpRequest.UsePassive = false;

            ftpRequest.Credentials = new NetworkCredential(uploadUserName, uploadPassword);

            var ff = new FileInfo(fileToUpload);
            var fileContents = new byte[ff.Length];

            using (FileStream fr = ff.OpenRead())
            {
                fr.Read(fileContents, 0, Convert.ToInt32(ff.Length));
            }

            using (Stream writer = ftpRequest.GetRequestStream())
            {
                writer.Write(fileContents, 0, fileContents.Length);
            }

            var ftpResponse = (FtpWebResponse)ftpRequest.GetResponse();
            string strResult = ftpResponse.StatusDescription;
            ftpResponse.Close();

            return strResult;
        }
        catch (Exception ex)
        {
            return ex.Message;
            //return "Error!!!";
        }
    }
    #endregion
}

public static class StringExt
{
    public static string Truncate(this string value, int maxLength)
    {
        if (string.IsNullOrEmpty(value)) return value;
        return value.Length <= maxLength ? value : value.Substring(0, maxLength) + "...";
    }
}