<%@ Application Language="C#" %>
<%@ Import Namespace="System.Web.Routing" %>
<%@ Import Namespace="System.Net" %>

<script runat="server">

   void Application_Start(object sender, EventArgs e)
   {
      // Code that runs on application startup
      RegisterRoutes(RouteTable.Routes);
      ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11;
   }

   void Application_End(object sender, EventArgs e)
   {
      //  Code that runs on application shutdown

   }

   void Application_Error(object sender, EventArgs e)
   {
      // Code that runs when an unhandled error occurs

   }

   void Session_Start(object sender, EventArgs e)
   {
      // Code that runs when a new session is started

   }

   void Session_End(object sender, EventArgs e)
   {
      // Code that runs when a session ends.
      // Note: The Session_End event is raised only when the sessionstate mode
      // is set to InProc in the Web.config file. If session mode is set to StateServer
      // or SQLServer, the event is not raised.

   }
   void RegisterRoutes(RouteCollection routes)
   {
      // backend
      routes.MapPageRoute("login", "login", "~/websystem/login.aspx");
      routes.MapPageRoute("warning", "warning", "~/websystem/warning.aspx");
      routes.MapPageRoute("error", "error", "~/websystem/error.aspx");
      routes.MapPageRoute("success", "success", "~/websystem/success.aspx");

      routes.MapPageRoute("sap", "sap/{file_name}", "~/websystem/sap/link_file.aspx", true, new RouteValueDictionary { { "file_name", "0" } });
   }
</script>